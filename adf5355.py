
from math import ceil
import time
from regs import *
from fractions import gcd


def DIV_ROUND_UP(x, y):
    a = int(ceil(x / y))
    if x%y != 0:
        a = a+1
    return a


def clamp(tmp, param, param1):
    return max(min(tmp, param1), param)

def int_to_bytes(value, length):
    result = []
    for i in range(0, length):
        result.append(int(value >> (i * 8) & 0xff))
    result.reverse()
    return result

class adf5355(object):
    class adf5355_state:
        clkin = 0
        fpfd = 0
        min_vco_freq = ADF4355_MIN_VCO_FREQ
        min_out_freq = ADF4355_MIN_OUT_FREQ
        max_out_freq = ADF4355_MAX_OUT_FREQ
        freq_req_chan = 0
        integer = 0
        fract1 = 0
        fract2 = 0
        mod2 = 0
        rf_div_sel = 0
        delay_us = 0
        regs = [0 for i in range(0, ADF5355_REG_NUM)]
        clock_shift = 0
        all_synced = 0
        is_5355 = True

    """
    
    """

    def __init__(self, spi, ref_div_factor=0, ref_doubler_en=False, ref_div2_en=False, cp_curr_uA=900,
                 mux_out_sel=ADF5355_MUXOUT_DIGITAL_LOCK_DETECT, ref_diff_en=False, mux_out_3V3_en=True,
                 phase_detector_polarity_neg=False, clkin=26000000, cp_gated_bleed_en=False, cp_neg_bleed_en=True,
                 mute_till_lock_detect_en=True, outa_en=True, outb_en=True, outa_power=0, outb_power=0, adc_clock = 154,
                 VCO_power=3, cp_bleed=24, ADC_cov=1, ADC_enable=1, Counter_Reset=0,
                 cp3_state=0, pwr_down=0, pd_polarity=0, mux_level=1, autocal=1, ref_mode=1, IF_auto=0
                 ):
        self.spi = spi
        self.cp_gated_bleed_en = cp_gated_bleed_en
        self.cp_neg_bleed_en = cp_neg_bleed_en
        self.mute_till_lock_detect_en = mute_till_lock_detect_en
        self.outb_en = outb_en
        self.outa_en = outa_en
        self.outa_power = outa_power
        self.outb_power = outb_power
        self.mux_out_sel = mux_out_sel
        self.ref_diff_en = ref_diff_en
        self.phase_detector_polarity_neg = phase_detector_polarity_neg
        self.cp_curr_uA = cp_curr_uA
        self.ref_div2_en = ref_div2_en
        self.ref_doubler_en = ref_doubler_en
        self.ref_div_factor = ref_div_factor
        self.mux_out_sel = mux_out_sel
        self.ref_diff_en = ref_diff_en
        self.mux_out_3V3_en = mux_out_3V3_en
        self.phase_detector_polarity_neg = phase_detector_polarity_neg
        self.cp_curr_uA = cp_curr_uA
        self.ref_div2_en = ref_div2_en
        self.ref_doubler_en = ref_doubler_en
        self.ref_div_factor = ref_div_factor
        self.adc_clock = adc_clock
        self.VCO_power = VCO_power
        self.cp_bleed = cp_bleed
        self.ADC_cov = ADC_cov
        self.ADC_enable = ADC_enable
        self.Counter_Reset = Counter_Reset
        self.cp3_state = cp3_state
        self.pwr_down = pwr_down
        self.pd_polarity = pd_polarity
        self.mux_level = mux_level
        self.autocal = autocal
        self.ref_mode = ref_mode
        self.IF_auto = IF_auto
        self.st = self.adf5355_state()
        self.st.clkin = clkin
        self.setup()
        print(self.st.clkin)

    def spi_write(self, value):
        bytes_to_send = int_to_bytes(value, 4)
        print "0x%08x" % value, value, bytes_to_send
        self.spi.writebytes(bytes_to_send)

    def pll_fract_n_compute(self, vco, pfd):

        tmp = vco % pfd
        vco /= pfd
        tmp = tmp * ADF5355_MODULUS1
        fract2 = tmp % pfd
        tmp /= pfd
        integer = vco
        fract1 = tmp
        mod2 = pfd

        gcd_div = gcd(fract2, 200000)
        mod2 /= gcd_div
        fract2 /= gcd_div

        return integer, fract1, fract2, mod2

    def setup(self):
        ref_div_factor = self.ref_div_factor

        while True:
            ref_div_factor += 1
            self.st.fpfd = (self.st.clkin * (2 if self.ref_doubler_en else 1)) / (
                ref_div_factor * (2 if self.ref_div2_en else 1))
            if self.st.fpfd <= 75000000:
                break

        tmp = int(round((self.cp_curr_uA - 315) / 315))
        tmp = max(min(tmp, 15), 0)
        self.st.regs[ADF5355_REG4] = ADF5355_REG4_COUNTER_RESET_EN(self.Counter_Reset) | \
                                     ADF5355_REG4_CP_THREESTATE_EN(self.cp3_state) | \
                                     ADF5355_REG4_POWER_DOWN_EN(self.pwr_down) | \
                                     ADF5355_REG4_PD_POLARITY_POS(self.pd_polarity) | \
                                     ADF5355_REG4_MUX_LOGIC(int(self.mux_level)) | \
                                     ADF5355_REG4_REFIN_MODE_DIFF(self.ref_mode) | \
                                     ADF5355_REG4_CHARGE_PUMP_CURR(2) | \
                                     ADF5355_REG4_DOUBLE_BUFF_EN(0) | \
                                     ADF5355_REG4_10BIT_R_CNT(ref_div_factor) | \
                                     ADF5355_REG4_RDIV2_EN(int(self.ref_div2_en)) | \
                                     ADF5355_REG4_RMULT2_EN(int(self.ref_doubler_en)) | \
                                     ADF5355_REG4_MUXOUT(self.mux_out_sel)

        self.st.regs[ADF5355_REG5] = ADF5355_REG5_DEFAULT

        self.st.regs[ADF5355_REG7] = ADF5355_REG7_LD_MODE_INT_N_EN(0) | \
                                     ADF5355_REG7_FACT_N_LD_PRECISION(3) | \
                                     ADF5355_REG7_LOL_MODE_EN(0) | \
                                     ADF5355_REG7_LD_CYCLE_CNT(0) | \
                                     ADF5355_REG7_LE_SYNCED_REFIN_EN(1) | \
                                     ADF5355_REG7_DEFAULT

        self.st.regs[ADF5355_REG8] = ADF5355_REG8_DEFAULT


        tmp = DIV_ROUND_UP(self.st.fpfd, 20000 * 30)
        print(self.st.fpfd)

        tmp = clamp(tmp, 1, 1023)

        self.st.regs[ADF5355_REG9] = ADF5355_REG9_TIMEOUT(103) | \
                                     ADF5355_REG9_SYNTH_LOCK_TIMEOUT(12) | \
                                     ADF5355_REG9_ALC_TIMEOUT(30) | \
                                     ADF5355_REG9_VCO_BAND_DIV(DIV_ROUND_UP(self.st.fpfd,2400000))

        tmp = int(ceil(((self.st.fpfd / 100000) - 2 ) / 4))
        if  ((self.st.fpfd / 100000) - 2 ) % 4 != 0 :
            tmp = tmp + 1
        tmp = clamp(tmp, 1, 255)

        if self.IF_auto == 1:
            self.adc_clock = DIV_ROUND_UP(float(self.st.fpfd/100000)-2, 4)+1


        self.st.delay_us = DIV_ROUND_UP(16000000, self.st.fpfd / (4 * tmp + 2))

        self.st.regs[ADF5355_REG10] = ADF5355_REG10_ADC_EN(self.ADC_enable) | \
                                      ADF5355_REG10_ADC_CONV_EN(self.ADC_cov) | \
                                      ADF5355_REG10_ADC_CLK_DIV(self.adc_clock) | \
                                      ADF5355_REG10_DEFAULT

        self.st.regs[ADF5355_REG11] = ADF5355_REG11_DEFAULT

        self.st.regs[ADF5355_REG12] = ADF5355_REG12_PHASE_RESYNC_CLK_DIV(1) | ADF5355_REG12_DEFAULT

        self.st.all_synced = False

    def set_freq(self, freq, channel):
        if channel == 0:
            if freq > self.st.max_out_freq or freq < self.st.min_out_freq:
                
                raise Exception('Invalid frequency!')
            self.st.rf_div_sel = 0
            while freq < self.st.min_vco_freq:
                freq <<= 1
                self.st.rf_div_sel += 1
        else:
            if freq > ADF5355_MAX_OUTB_FREQ or freq < ADF5355_MIN_OUTB_FREQ:
                raise Exception('Invalid frequency!')
            freq >>= 1

        self.st.integer, self.st.fract1, self.st.fract2, self.st.mod2 = self.pll_fract_n_compute(freq, self.st.fpfd)
        #print "self.st.integer %u, self.st.fract1 %u, self.st.fract2 %u, self.st.mod2 %u, freq %u, self.st.fpfd %u, rf_div_sel %u" % (self.st.integer, self.st.fract1, self.st.fract2, self.st.mod2, freq, self.st.fpfd, self.st.rf_div_sel)
        prescaler = (self.st.integer >= ADF5355_MIN_INT_PRESCALER_89)


        if self.IF_auto == 1:
            self.cp_bleed = int((self.st.fpfd/61440000)*24)
        

        self.st.regs[ADF5355_REG0] = ADF5355_REG0_INT(self.st.integer) | \
                                     ADF5355_REG0_PRESCALER(prescaler) | ADF5355_REG0_AUTOCAL(self.autocal)
        self.st.regs[ADF5355_REG1] = ADF5355_REG1_FRACT(self.st.fract1)
        self.st.regs[ADF5355_REG2] = ADF5355_REG2_MOD2(self.st.mod2) | ADF5355_REG2_FRAC2(self.st.fract2)

        self.st.regs[ADF5355_REG6] = ADF5355_REG6_OUTPUT_PWR(self.VCO_power) | \
                                     ADF5355_REG6_RF_OUT_EN(int(self.outa_en)) | \
                                     (ADF5355_REG6_RF_OUTB_EN(
                                         int(not self.outb_en)) if self.st.is_5355 else ADF4355_REG6_OUTPUTB_PWR( self.outb_power) | ADF4355_REG6_RF_OUTB_EN(int(self.outb_en))) | \
                                     ADF5355_REG6_MUTE_TILL_LOCK_EN(0) | \
                                     ADF5355_REG6_CP_BLEED_CURR(self.cp_bleed) | \
                                     ADF5355_REG6_RF_DIV_SEL(self.st.rf_div_sel) | \
                                     ADF5355_REG6_FEEDBACK_FUND(1) | \
                                     ADF5355_REG6_NEG_BLEED_EN(int(self.cp_neg_bleed_en)) | \
                                     ADF5355_REG6_GATED_BLEED_EN(int(self.cp_gated_bleed_en)) | \
                                     ADF5355_REG6_DEFAULT

        self.st.freq_req = freq
        self.st.freq_req_chan = channel

        for i in  range(ADF5355_REG_NUM -1, ADF5355_REG0 - 1, -1):
                self.spi_write(self.st.regs[i] | i)

