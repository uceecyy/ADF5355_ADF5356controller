from math import ceil
import time

from fractions import gcd


# /* REG0 Bit Definitions */
def ADF5356_REG0_INT(x):
    return (((x) & 0xFFFF) << 4)


def ADF5355_REG0_PRESCALER(x):
    return ((x) << 20)


def ADF5355_REG0_AUTOCAL(x):
    return ((x) << 21)


# /* REG1 Bit Definitions */
def ADF5355_REG1_FRACT(x):
    return (((x) & 0xFFFFFF) << 4)


# /* REG2 Bit Definitions */
def ADF5355_REG2_MOD2(x):
    return (((x) & 0x3FFF) << 4)


def ADF5355_REG2_FRAC2(x):
    return (((x) & 0x3FFF) << 18)


# /* REG3 Bit Definitions */
def ADF5355_REG3_PHASE(x):
    return (((x) & 0xFFFFFF) << 4)


def ADF5355_REG3_PHASE_ADJUST(x):
    return ((x) << 28)


def ADF5355_REG3_PHASE_RESYNC(x):
    return ((x) << 29)


def ADF5355_REG3_EXACT_SDLOAD_RESET(x):
    return ((x) << 30)


# /* REG4 Bit Definitions */
def ADF5355_REG4_COUNTER_RESET_EN(x):
    return ((x) << 4)


def ADF5355_REG4_CP_THREESTATE_EN(x):
    return ((x) << 5)


def ADF5355_REG4_POWER_DOWN_EN(x):
    return ((x) << 6)


def ADF5355_REG4_PD_POLARITY_POS(x):
    return ((x) << 7)


def ADF5355_REG4_MUX_LOGIC(x):
    return ((x) << 8)


def ADF5355_REG4_REFIN_MODE_DIFF(x):
    return ((x) << 9)


def ADF5355_REG4_CHARGE_PUMP_CURR(x):
    return (((x) & 0xF) << 10)


def ADF5355_REG4_DOUBLE_BUFF_EN(x):
    return ((x) << 14)


def ADF5355_REG4_10BIT_R_CNT(x):
    return (((x) & 0x3FF) << 15)


def ADF5355_REG4_RDIV2_EN(x):
    return ((x) << 25)


def ADF5355_REG4_RMULT2_EN(x):
    return ((x) << 26)


def ADF5355_REG4_MUXOUT(x):
    return (((x) & 0x7) << 27)


ADF5355_MUXOUT_THREESTATE = 0
ADF5355_MUXOUT_DVDD = 1
ADF5355_MUXOUT_GND = 2
ADF5355_MUXOUT_R_DIV_OUT = 3
ADF5355_MUXOUT_N_DIV_OUT = 4
ADF5355_MUXOUT_ANALOG_LOCK_DETECT = 5
ADF5355_MUXOUT_DIGITAL_LOCK_DETECT = 6

# /* REG5 Bit Definitions */
ADF5355_REG5_DEFAULT = 0x00800025


# /* REG6 Bit Definitions */
def ADF4355_REG6_OUTPUTB_PWR(x):
    return (((x) & 0x7) << 4)


def ADF4355_REG6_RF_OUTB_EN(x):
    return ((x) << 9)


def ADF5355_REG6_OUTPUT_PWR(x):
    return (((x) & 0x3) << 4)


def ADF5355_REG6_RF_OUT_EN(x):
    return ((x) << 6)


def ADF5355_REG6_RF_OUTB_EN(x):
    return ((x) << 10)


def ADF5355_REG6_MUTE_TILL_LOCK_EN(x):
    return ((x) << 11)


def ADF5355_REG6_CP_BLEED_CURR(x):
    return (((x) & 0xFF) << 13)


def ADF5355_REG6_RF_DIV_SEL(x):
    return (((x) & 0x7) << 21)


def ADF5355_REG6_FEEDBACK_FUND(x):
    return ((x) << 24)


def ADF5355_REG6_NEG_BLEED_EN(x):
    return ((x) << 29)


def ADF5355_REG6_GATED_BLEED_EN(x):
    return ((x) << 30)


ADF5355_REG6_DEFAULT = 0x14000006


# /* REG7 Bit Definitions */
def ADF5355_REG7_LD_MODE_INT_N_EN(x):
    return ((x) << 4)


def ADF5355_REG7_FACT_N_LD_PRECISION(x):
    return (((x) & 0x3) << 5)


def ADF5355_REG7_LOL_MODE_EN(x):
    return ((x) << 7)


def ADF5355_REG7_LD_CYCLE_CNT(x):
    return (((x) & 0x3) << 8)


def ADF5355_REG7_LE_SYNCED_REFIN_EN(x):
    return ((x) << 25)


ADF5355_REG7_DEFAULT = 0x60000E7

# /* REG8 Bit Definitions */
ADF5355_REG8_DEFAULT = 0x15596568


# /* REG9 Bit Definitions */
def ADF5355_REG9_SYNTH_LOCK_TIMEOUT(x):
    return (((x) & 0x1F) << 4)


def ADF5355_REG9_ALC_TIMEOUT(x):
    return (((x) & 0x1F) << 9)


def ADF5355_REG9_TIMEOUT(x):
    return (((x) & 0x3FF) << 14)


def ADF5355_REG9_VCO_BAND_DIV(x):
    return (((x) & 0xFF) << 24)


# /* REG10 Bit Definitions */
def ADF5355_REG10_ADC_EN(x):
    return ((x) << 4)


def ADF5355_REG10_ADC_CONV_EN(x):
    return ((x) << 5)


def ADF5355_REG10_ADC_CLK_DIV(x):
    return (((x) & 0xFF) << 6)


ADF5355_REG10_DEFAULT = 0x00C0000A

# /* REG11 Bit Definitions */
ADF5355_REG11_DEFAULT = 0x0061200B


# /* REG12 Bit Definitions */
def ADF5355_REG12_PHASE_RESYNC_CLK_DIV(x):
    return (((x) & 0xFFFFF) << 20)


ADF5355_REG12_DEFAULT = 0x000015FC

# /* Specifications */
ADF5355_MIN_VCO_FREQ = 3400000000  # /* Hz */
ADF5355_MAX_VCO_FREQ = 6800000000  # /* Hz */
ADF5355_MAX_OUT_FREQ = ADF5355_MAX_VCO_FREQ  # /* Hz */
ADF5355_MIN_OUT_FREQ = (ADF5355_MIN_VCO_FREQ / 64)  # /* Hz */
ADF5355_MAX_OUTB_FREQ = (ADF5355_MAX_VCO_FREQ * 2)  # /* Hz */
ADF5355_MIN_OUTB_FREQ = (ADF5355_MIN_VCO_FREQ * 2)  # /* Hz */

ADF5355_MAX_FREQ_PFD = 125000000  # /* Hz */
ADF5355_MAX_FREQ_REFIN = 600000000  # /* Hz */
ADF5355_MAX_MODUS2 = 16384
ADF5355_MAX_R_CNT = 1023

ADF5355_MODULUS1 = 16777216
ADF5355_MIN_INT_PRESCALER_89 = 75

ADF5355_REG0 = 0
ADF5355_REG1 = 1
ADF5355_REG2 = 2
ADF5355_REG3 = 3
ADF5355_REG4 = 4
ADF5355_REG5 = 5
ADF5355_REG6 = 6
ADF5355_REG7 = 7
ADF5355_REG8 = 8
ADF5355_REG9 = 9
ADF5355_REG10 = 10
ADF5355_REG11 = 11
ADF5355_REG12 = 12
ADF5355_REG_NUM = 13

def DIV_ROUND_UP(x, y):
    a = int(ceil(x / y))
    if x%y != 0:
        a = a+1
    return a


def clamp(tmp, param, param1):
    return max(min(tmp, param1), param)

def bytes_converter(value):
    bytesgroup = []
    for i in range(0, 4):
        bytesgroup.append(int(value >> (i * 8) & 0xff))
    bytesgroup.reverse()

    return bytesgroup

class adf5356(object):
    class adf5356_state:
        clkin = 0
        fpfd = 0
        min_vco_freq = 3400000000
        min_out_freq = 3400000000
        max_out_freq = 6800000000
        freq_req_chan = 0
        integer = 0
        fract1 = 0
        fract2 = 0
        mod2 = 0
        rf_div_sel = 0
        delay_us = 0
        regs = [0 for i in range(0, ADF5355_REG_NUM)]
        clock_shift = 0
        all_synced = 0
        is_5355 = True

    def __init__(self, spi, ref_div_factor=0, ref_doubler_en=False, ref_div2_en=False, cp_curr_uA=900,
                 mux_out_sel=ADF5355_MUXOUT_DIGITAL_LOCK_DETECT, ref_diff_en=False, mux_out_3V3_en=True,
                 phase_detector_polarity_neg=False, clkin=26000000,
                 cp_gated_bleed_en=False, cp_neg_bleed_en=True,
                 mute_till_lock_detect_en=True, outa_en=True, outb_en=True,
                 outa_power=0, outb_power=0, adc_clock = 154,
                 VCO_power=3, cp_bleed=24, ADC_cov=1, ADC_enable=1, Counter_Reset=0,
                 cp3_state=0, pwr_down=0, pd_polarity=0, mux_level=1, autocal=1, ref_mode=1, IF_auto=0
                 ):
        self.spi = spi
        self.cp_gated_bleed_en = cp_gated_bleed_en
        self.cp_neg_bleed_en = cp_neg_bleed_en
        self.mute_till_lock_detect_en = mute_till_lock_detect_en
        self.outb_en = outb_en
        self.outa_en = outa_en
        self.outa_power = outa_power
        self.outb_power = outb_power
        self.mux_out_sel = mux_out_sel
        self.ref_diff_en = ref_diff_en
        self.mux_out_3V3_en = mux_out_3V3_en
        self.phase_detector_polarity_neg = phase_detector_polarity_neg
        self.cp_curr_uA = cp_curr_uA
        self.ref_div2_en = ref_div2_en
        self.ref_doubler_en = ref_doubler_en
        self.ref_div_factor = ref_div_factor
        self.adc_clock = adc_clock
        self.VCO_power = VCO_power
        self.cp_bleed = cp_bleed
        self.ADC_cov = ADC_cov
        self.ADC_enable = ADC_enable
        self.Counter_Reset = Counter_Reset
        self.cp3_state = cp3_state
        self.pwr_down = pwr_down
        self.pd_polarity = pd_polarity
        self.mux_level = mux_level
        self.autocal = autocal
        self.ref_mode = ref_mode
        self.IF_auto = IF_auto
        self.st = self.adf5356_state()
        self.st.clkin = clkin
        self.setup()
        
    def spi_write(self, value):
        bytes_to_send = bytes_converter(value, 4)
        self.spi.writebytes(bytes_to_send)
           
    def calculator(self, vco, pfd):

        tmp = vco % pfd
        vco /= pfd
        tmp = tmp * ADF5355_MODULUS1
        fract2 = tmp % pfd
        tmp /= pfd
        integer = vco
        fract1 = tmp
        mod2 = pfd

        gcd_div = gcd(fract2, 200000)
        mod2 /= gcd_div
        fract2 /= gcd_div

        return integer, fract1, fract2, mod2
    
    def setup(self):
        ref_div_factor = self.ref_div_factor

        while True:
            
            ref_div_factor += 1
            self.st.fpfd = (self.st.clkin * (2 if self.ref_doubler_en else 1)) / (
                ref_div_factor * (2 if self.ref_div2_en else 1))
            if self.st.fpfd <= 75000000:
                break

       
        self.st.regs[ADF5355_REG4] = ADF5355_REG4_COUNTER_RESET_EN(self.Counter_Reset) | \
                                     ADF5355_REG4_CP_THREESTATE_EN(self.cp3_state) | \
                                     ADF5355_REG4_POWER_DOWN_EN(self.pwr_down) | \
                                     ADF5355_REG4_PD_POLARITY_POS(self.pd_polarity) | \
                                     ADF5355_REG4_MUX_LOGIC(self.mux_level) | \
                                     ADF5355_REG4_REFIN_MODE_DIFF(self.ref_mode) | \
                                     ADF5355_REG4_CHARGE_PUMP_CURR(2) | \
                                     ADF5355_REG4_DOUBLE_BUFF_EN(0) | \
                                     ADF5355_REG4_10BIT_R_CNT(ref_div_factor) | \
                                     ADF5355_REG4_RDIV2_EN(int(self.ref_div2_en)) | \
                                     ADF5355_REG4_RMULT2_EN(int(self.ref_doubler_en)) | \
                                     ADF5355_REG4_MUXOUT(self.mux_out_sel)

        self.st.regs[ADF5355_REG5] = ADF5355_REG5_DEFAULT

        self.st.regs[ADF5355_REG7] = ADF5355_REG7_LD_MODE_INT_N_EN(0) | \
                                     ADF5355_REG7_FACT_N_LD_PRECISION(3) | \
                                     ADF5355_REG7_LOL_MODE_EN(0) | \
                                     ADF5355_REG7_LD_CYCLE_CNT(0) | \
                                     ADF5355_REG7_LE_SYNCED_REFIN_EN(1) | \
                                     ADF5355_REG7_DEFAULT

        self.st.regs[ADF5355_REG8] = ADF5355_REG8_DEFAULT

        

        self.st.regs[ADF5355_REG9] = ADF5355_REG9_TIMEOUT(103) | \
                                     ADF5355_REG9_SYNTH_LOCK_TIMEOUT(12) | \
                                     ADF5355_REG9_ALC_TIMEOUT(30) | \
                                     ADF5355_REG9_VCO_BAND_DIV(DIV_ROUND_UP(self.st.fpfd,1600000))
        
        if self.IF_auto == 1:
            self.adc_clock = DIV_ROUND_UP(float(self.st.fpfd/100000)-2, 4)+1
            
        
        self.st.delay_us = int(20 * float(10000000 / float(self.st.fpfd / self.adc_clock)))
        
        self.st.regs[ADF5355_REG10] = ADF5355_REG10_ADC_EN(self.ADC_enable) | \
                                      ADF5355_REG10_ADC_CONV_EN(self.ADC_cov) | \
                                      ADF5355_REG10_ADC_CLK_DIV(self.adc_clock) | \
                                      ADF5355_REG10_DEFAULT

        self.st.regs[ADF5355_REG11] = ADF5355_REG11_DEFAULT

        self.st.regs[ADF5355_REG12] = ADF5355_REG12_PHASE_RESYNC_CLK_DIV(0) | ADF5355_REG12_DEFAULT


    def set_freq(self, freq, channel):
        if channel == 0:
            if freq > self.st.max_out_freq or freq < self.st.min_out_freq:
                
                raise Exception('Invalid frequency!')
            self.st.rf_div_sel = 0
            while freq < self.st.min_vco_freq:
                freq <<= 1
                self.st.rf_div_sel += 1
        else:
            if freq > ADF5355_MAX_OUTB_FREQ or freq < ADF5355_MIN_OUTB_FREQ:
                raise Exception('Invalid frequency!')
            freq >>= 1

        self.st.integer, self.st.fract1, self.st.fract2, self.st.mod2 = self.calculator(freq, self.st.fpfd)
        prescaler = (self.st.integer >= ADF5355_MIN_INT_PRESCALER_89)

        if self.IF_auto == 1:
            self.cp_bleed = int((self.st.fpfd/61440000)*24)
        print(self.cp_bleed)
        

        self.st.regs[ADF5355_REG0] = ADF5355_REG0_INT(self.st.integer) | \
                                     ADF5355_REG0_PRESCALER(prescaler) | ADF5355_REG0_AUTOCAL(self.autocal)
        self.st.regs[ADF5355_REG1] = ADF5355_REG1_FRACT(self.st.fract1)
        self.st.regs[ADF5355_REG2] = ADF5355_REG2_MOD2(self.st.mod2) | ADF5355_REG2_FRAC2(self.st.fract2)

        self.st.regs[ADF5355_REG6] = ADF5355_REG6_OUTPUT_PWR(self.VCO_power) | \
                                     ADF5355_REG6_RF_OUT_EN(int(self.outa_en)) | \
                                     (ADF5355_REG6_RF_OUTB_EN(
                                         int(not self.outb_en)) if self.st.is_5355 else ADF4355_REG6_OUTPUTB_PWR( self.outb_power) | \
                                     ADF4355_REG6_RF_OUTB_EN(int(self.outb_en))) | \
                                     ADF5355_REG6_MUTE_TILL_LOCK_EN(0) | \
                                     ADF5355_REG6_CP_BLEED_CURR(self.cp_bleed) | \
                                     ADF5355_REG6_RF_DIV_SEL(self.st.rf_div_sel) | \
                                     ADF5355_REG6_FEEDBACK_FUND(1) | \
                                     ADF5355_REG6_NEG_BLEED_EN(int(self.cp_neg_bleed_en)) | \
                                     ADF5355_REG6_GATED_BLEED_EN(int(self.cp_gated_bleed_en)) | \
                                     ADF5355_REG6_DEFAULT

        self.st.freq_req = freq
        self.st.freq_req_chan = channel

        self.spi_write(0xD)
        for i in  range(ADF5355_REG_NUM -1, ADF5355_REG0 - 1, -1):
                self.spi_write(self.st.regs[i] | i)

