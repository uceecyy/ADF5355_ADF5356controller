import Tkinter as tk
import sys
from math import floor
from Tkinter import *
from ttk import *
from Tkinter import Tk, Frame, Menu
import tkFont as tkfont
from adf5355 import adf5355
from adf5356 import adf5356
import spidev, ttk
from time import sleep

spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 4000000


def click_QuickStart():
    quickstart_window = Tk()
    quickstart_window.title("QuickStart")
    
    lbl_0 = Label(quickstart_window,text="output frequency")
    lbl_0.grid(column=0, row=0)
    lbl_1 = Label(quickstart_window,text="output selection")
    lbl_1.grid(column=0, row=1)
    lbl_2 = Label(quickstart_window,text="Reference frequency")
    lbl_2.grid(column=0, row=2)
    lbl_3 = Label(quickstart_window,text="Device in use")
    lbl_3.grid(column=0, row=3)
        
    input_0 = Entry(quickstart_window,width=10)
    input_0.grid(column=1, row=0)
    input_1 = Entry(quickstart_window,width=10)
    input_1.grid(column=1, row=1)
    input_2 = Entry(quickstart_window,width=10)
    input_2.grid(column=1, row=2)
    device_value = tk.StringVar()
    device_in_use = ttk.Combobox(quickstart_window, width=8,
                              textvariable = device_value)
    device_in_use['values'] = ("ADF5355", "ADF5356")
    device_in_use.grid(column=1, row=3)
    device_in_use.current(0)
    device_in_use.set("ADF5355")
    
    lbl_01 = Label(quickstart_window,text="MHz")
    lbl_01.grid(column=2, row=0)
    lbl_11 = Label(quickstart_window,text="0: RF_A 1: RF_B")
    lbl_11.grid(column=2, row=1)
    lbl_12 = Label(quickstart_window,text="MHz")
    lbl_12.grid(column=2, row=2)
    
    counter = 0
        
    def clicked_quick():
        out_freq = int(float(input_0.get()) * 1000000)
        channel = int(input_1.get())
        fref = int(float(input_2.get()) * 1000000)
        device = device_in_use.get()
                        
        if channel == 0 :
            outb_en = False
            outa_en = True
        else:
            outb_en = True
            outa_en = False

        
        if counter == 0:
            if device == "ADF5355":
                
                d = adf5355(spi, mute_till_lock_detect_en=True,
                        ref_doubler_en=False, clkin=fref, outb_en = True,
                        outa_en = True, mux_out_sel = 6, adc_clock=30)
                d.set_freq(out_freq, channel)
            else:
                
                d = adf5356(spi, mute_till_lock_detect_en=True,
                        ref_doubler_en=False, clkin=fref, outb_en = True,
                        outa_en = True, mux_out_sel = 6, adc_clock=30)
            
                d.set_freq(out_freq, channel)
                
        else:
            d.outa_en = outa_en
            d.outb_en = outb_en
            d.set_freq(out_freq, channel)
                
    btn = Button(quickstart_window,text="start",command=clicked_quick)
    btn.grid(column=1, row=4)
    
    quickstart_window.mainloop()

def click_MainControls():

    mainControl_window = tk.Tk()
    mainControl_window.title("ADF5356 Main Controls")
    #reg 6
    frame_reg6 = tk.Frame(mainControl_window, height=100, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg6.pack(fill=X, padx=2, pady=2)
    frame_reg6.place(x=0,y=75)

    lbl_REG6 = tk.Label(frame_reg6,text="Register 6")
    lbl_REG6.grid(column=0, row=0)
    lbl_RFA_POWER = tk.Label(frame_reg6,text="RFoutA Power:")
    lbl_RFA_POWER.grid(column=0, row=1)
    lbl_RFA_Enable = tk.Label(frame_reg6,text="RFoutA Enable: ")
    lbl_RFA_Enable.grid(column=0, row=2)
    lbl_RFB_Enable = tk.Label(frame_reg6,text="RFoutB Enable:")
    lbl_RFB_Enable.grid(column=0, row=3)
    lbl_BLEED = tk.Label(frame_reg6,text="Bleed Current:")
    lbl_BLEED.grid(column=0, row=4)
    lbl_BLEED_1 = tk.Label(frame_reg6,text=" x 3.75uA")
    lbl_BLEED_1.grid(column=1, row=5)

    power = tk.StringVar()
    PowerChosen = ttk.Combobox(frame_reg6, width = 10, textvariable = power)
    PowerChosen['values'] = ("-4dBm", "-1dBm", "+2dBm", "+5dBm")
    PowerChosen.grid(column=1, row=1)
    PowerChosen.current(3)
    If_Enable_A = tk.StringVar()
    RFA_Enable = ttk.Combobox(frame_reg6, width = 10,
                              textvariable = If_Enable_A)
    RFA_Enable['values'] = ("Disabled", "Enabled")
    RFA_Enable.grid(column=1, row=2)
    RFA_Enable.current(1)
    If_Enable_B = tk.StringVar()
    RFB_Enable = ttk.Combobox(frame_reg6, width = 10,
                              textvariable = If_Enable_B)
    RFB_Enable['values'] = ("Enabled", "Disabled")
    RFB_Enable.grid(column=1, row=3)
    RFB_Enable.current(0)

    input_bleed = tk.Entry(frame_reg6,width=10)
    input_bleed.insert(END, '24')
    input_bleed.grid(column=0, row=5)

    
    #reg 10
    frame_reg10 = tk.Frame(mainControl_window, relief=RAISED, colormap="new",
                           bd=1, padx=3, pady=3)
    frame_reg10.pack(fill=X, padx=2, pady=2)
    frame_reg10.place(x=210,y=152)
    
    lbl_REG10 = tk.Label(frame_reg10,text="Register 10")
    lbl_REG10.grid(column=0, row=0)
    lbl_adc_clk = tk.Label(frame_reg10,text="ADC Clock:")
    lbl_adc_clk.grid(column=0, row=1)
    lbl_adc_cov = tk.Label(frame_reg10,text="ADC Conversion:")
    lbl_adc_cov.grid(column=0, row=2)
    lbl_adc_En = tk.Label(frame_reg10,text="ADC Enable:")
    lbl_adc_En.grid(column=0, row=3)
    
    input_ADC_clk = tk.Entry(frame_reg10,width=10)
    input_ADC_clk.insert(END, '154')
    input_ADC_clk.grid(column=1, row=1)

    
    radio_value_ADC_cov = IntVar()
    radio_value_ADC_cov.set(1)
    def sel_adc_cov_on():
        radio_value_ADC_cov.set(1)
        
    def sel_adc_cov_off():
        radio_value_ADC_cov.set(0)
    
    Radio_ADC_cov_on = tk.Radiobutton(frame_reg10, text="Enabled",
                                      variable = radio_value_ADC_cov, value=1,
                                      command=sel_adc_cov_on)
    Radio_ADC_cov_on.grid(column=1, row=2, sticky="w")
    Radio_ADC_cov_on.select()
    Radio_ADC_cov_off = tk.Radiobutton(frame_reg10, text="Disabled",
                                       variable = radio_value_ADC_cov, value=0,
                                       command=sel_adc_cov_off)
    Radio_ADC_cov_off.grid(column=2, row=2, sticky="w")
    Radio_ADC_cov_off.deselect()
    
    radio_value_ADC_En = IntVar()
    radio_value_ADC_En.set(1)
    def sel_adc_en_on():
        radio_value_ADC_En.set(1)
        
    def sel_adc_en_off():
        radio_value_ADC_En.set(0)
        
    Radio_ADC_En_on = tk.Radiobutton(frame_reg10, text="Enabled",
                                     variable = radio_value_ADC_En, value=1,
                                     command=sel_adc_en_on)
    Radio_ADC_En_on.grid(column=1, row=3, sticky="w")
    Radio_ADC_En_on.select()
    Radio_ADC_En_off = tk.Radiobutton(frame_reg10, text="Disabled",
                                      variable = radio_value_ADC_En, value=0,
                                      command=sel_adc_en_off)
    Radio_ADC_En_off.grid(column=2, row=3, sticky="w")
    Radio_ADC_En_off.deselect()
    
    #reg4

    frame_reg4 = tk.Frame(mainControl_window, height=100, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg4.pack(fill=X, padx=2, pady=2)
    frame_reg4.place(x=210,y=0)

    lbl_REG4 = tk.Label(frame_reg4,text="Register 4")
    lbl_REG4.grid(column=0, row=0)
    lbl_Counter_re = tk.Label(frame_reg4,text="Counter reset:")
    lbl_Counter_re.grid(column=0, row=1)
    lbl_CP3_state = tk.Label(frame_reg4,text="CP3-state:")
    lbl_CP3_state.grid(column=0, row=2)
    lbl_Powerdown = tk.Label(frame_reg4,text="Powerdown:")
    lbl_Powerdown.grid(column=0, row=3)
    lbl_PD = tk.Label(frame_reg4,text="PD Polarity:")
    lbl_PD.grid(column=0, row=4)
    lbl_Mux_level = tk.Label(frame_reg4,text="Mux level:")
    lbl_Mux_level.grid(column=0, row=5)
    lbl_RF_mod = tk.Label(frame_reg4,text="REFin Mode:")
    lbl_RF_mod.grid(column=0, row=6)

    radio_value_CT_RE = IntVar()
    radio_value_CT_RE.set(0)
    def sel_ct_re_on():
        radio_value_CT_RE.set(1)
        
    def sel_ct_re_off():
        radio_value_CT_RE.set(0)
        
    Radio_CT_Re_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                    variable = radio_value_CT_RE, value=1,
                                    command=sel_ct_re_on)
    Radio_CT_Re_on.grid(column=1, row=1, sticky="w")
    Radio_CT_Re_on.deselect()
    Radio_CT_Re_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                     variable = radio_value_CT_RE, value=0,
                                     command=sel_ct_re_off)
    Radio_CT_Re_off.grid(column=2, row=1, sticky="w")
    Radio_CT_Re_off.select()
    
    radio_value_CP = IntVar()
    radio_value_CP.set(0)
    def sel_cp_on():
        radio_value_CP.set(1)
        
    def sel_cp_off():
        radio_value_CP.set(0)
    
    Radio_CP_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                 variable = radio_value_CP, value=1,
                                 command=sel_cp_on)
    Radio_CP_on.grid(column=1, row=2, sticky="w")
    Radio_CP_on.deselect()
    Radio_CP_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                  variable = radio_value_CP, value=0,
                                  command=sel_cp_off)
    Radio_CP_off.grid(column=2, row=2, sticky="w")
    Radio_CP_off.select()
    
    radio_value_PWR_down = IntVar()
    radio_value_PWR_down.set(0)
    def sel_PWR_down_on():
        radio_value_PWR_down.set(1)
        
    def sel_PWR_down_off():
        radio_value_PWR_down.set(0)
    
    Radio_PWR_down_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                       variable = radio_value_PWR_down,
                                       value=1, command=sel_PWR_down_on)
    Radio_PWR_down_on.grid(column=1, row=3, sticky="w")
    Radio_PWR_down_on.deselect()
    Radio_PWR_down_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                        variable = radio_value_PWR_down,
                                        value=0, command=sel_PWR_down_off)
    Radio_PWR_down_off.grid(column=2, row=3, sticky="w")
    Radio_PWR_down_off.select()

    radio_value_PD = IntVar()
    radio_value_PD.set(0)
    def sel_PD_positive():
        radio_value_PD.set(1)
        
    def sel_PD_negative():
        radio_value_PD.set(0)
    
    Radio_PD_positive = tk.Radiobutton(frame_reg4, text="Positive",
                                       variable = radio_value_PD, value=1,
                                       command=sel_PD_positive)
    Radio_PD_positive.grid(column=1, row=4, sticky="w")
    Radio_PD_positive.deselect()
    Radio_PD_negative = tk.Radiobutton(frame_reg4, text="Negative",
                                       variable = radio_value_PD, value=0,
                                       command=sel_PD_negative)
    Radio_PD_negative.grid(column=2, row=4, sticky="w")
    Radio_PD_negative.select()

    radio_value_Mux_level = IntVar()
    radio_value_Mux_level.set(1)
    def sel_Mux_level_3v3():
        radio_value_Mux_level.set(1)
        
    def sel_Mux_level_1v8():
        radio_value_Mux_level.set(0)
    
    Radio_Mux_level_3v3 = tk.Radiobutton(frame_reg4, text="3.3V",
                                         variable = radio_value_Mux_level,
                                         value=1, command=sel_Mux_level_3v3)
    Radio_Mux_level_3v3.grid(column=1, row=5, sticky="w")
    Radio_Mux_level_3v3.select()
    Radio_Mux_level_1v8 = tk.Radiobutton(frame_reg4, text="1.8V",
                                         variable = radio_value_Mux_level,
                                         value=0, command=sel_Mux_level_1v8)
    Radio_Mux_level_1v8.grid(column=2, row=5, sticky="w")
    Radio_Mux_level_1v8.deselect()

    radio_value_RF_mod = IntVar()
    radio_value_RF_mod.set(1)
    def sel_RF_mod_diff():
        radio_value_RF_mod.set(1)
        
    def sel_RF_mod_single():
        radio_value_RF_mod.set(0)
    
    Radio_RF_mod_diff = tk.Radiobutton(frame_reg4, text="Differential",
                                         variable = radio_value_RF_mod,
                                         value=1, command=sel_RF_mod_diff)
    Radio_RF_mod_diff.grid(column=1, row=6, sticky="w")
    Radio_RF_mod_diff.select()
    Radio_RF_mod_single = tk.Radiobutton(frame_reg4, text="Single",
                                         variable = radio_value_RF_mod,
                                         value=0, command=sel_RF_mod_single)
    Radio_RF_mod_single.grid(column=2, row=6, sticky="w")
    Radio_RF_mod_single.deselect()

    
    #reg0
    frame_reg0 = tk.Frame(mainControl_window, width=200, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg0.pack(fill=X, padx=2, pady=2)
    frame_reg0.place(x=0,y=200)
    lbl_REG0 = tk.Label(frame_reg0,text="Register 0")
    lbl_REG0.grid(column=0, row=0)
    lbl_Autocal = tk.Label(frame_reg0,text="      Autocal:          ")
    lbl_Autocal.grid(column=0, row=1)
    
    radio_value_Autocal = IntVar()
    radio_value_Autocal.set(1)
    def sel_Autocal_en():
        radio_value_Autocal.set(1)
        
    def sel_Autocal_dis():
        radio_value_Autocal.set(0)
    
    Radio_Autocal_en = tk.Radiobutton(frame_reg0, text="Enabled",
                                         variable = radio_value_Autocal,
                                         value=1, command=sel_Autocal_en)
    Radio_Autocal_en.grid(column=1, row=1, sticky="w")
    Radio_Autocal_en.select()
    Radio_Autocal_dis = tk.Radiobutton(frame_reg0, text="Disabled",
                                         variable = radio_value_Autocal,
                                         value=0, command=sel_Autocal_dis)
    Radio_Autocal_dis.grid(column=1, row=2, sticky="w")
    Radio_Autocal_dis.deselect()

    
    
    
    #rf settings
    frame_rf = tk.Frame(mainControl_window, height=100, relief=RAISED,
                        colormap="new", bd=1, padx=3, pady=3)
    frame_rf.pack(fill=X, padx=2, pady=2)
    frame_rf.place(x=0,y=0)
    lbl_RF_setting = tk.Label(frame_rf,text="RF Settings")
    lbl_RF_setting.grid(column=0, row=0)
    lbl_reference_freq = tk.Label(frame_rf,text="Reference freq:")
    lbl_reference_freq.grid(column=0, row=1)
    input_ref = tk.Entry(frame_rf,width=8)
    input_ref.insert(END, '122.88')
    input_ref.grid(column=1, row=1)
    lbl_MHz = tk.Label(frame_rf,text="MHz")
    lbl_MHz.grid(column=2, row=1)
    lbl_VCO_out = tk.Label(frame_rf,text="VCO out:")
    lbl_VCO_out.grid(column=0, row=2)
    input_vco = tk.Entry(frame_rf,width=8)
    input_vco.grid(column=1, row=2)
    lbl_MHz_1 = tk.Label(frame_rf,text="MHz")
    lbl_MHz_1.grid(column=2, row=2)

    counter = 0
        
    def clicked():
        if_auto = radio_value_Automatic.get()
            
        out_freq = int(float(input_vco.get()) * 1000000)
        ADC_CLK = int(input_ADC_clk.get())
        bleed_cp = int(input_bleed.get())
        ADC_CONV = radio_value_ADC_cov.get()
        ADC_En = radio_value_ADC_En.get()
        fref = int(float(input_ref.get()) * 1000000)
        CT_RE = radio_value_CT_RE.get()
        CP_STATE = radio_value_CP.get()
        PWR_DOWN = radio_value_PWR_down.get()
        PD_polarity = radio_value_PD.get()
        Mux_level = radio_value_Mux_level.get()
        Autocal = radio_value_Autocal.get()
        REF_MODE = radio_value_RF_mod.get()
        
        if RFA_Enable.get() == "Enabled":
            if_RF_A = True
        else:
            if_RF_A = False

        if PowerChosen.get() == "-4dBm":
            vco_pwr = 0
        elif PowerChosen.get() == "-1dBm":
            vco_pwr = 1
        elif PowerChosen.get() == "+2dBm":
            vco_pwr = 2
        elif PowerChosen.get() == "+5dBm":
            vco_pwr = 3
        
        if RFA_Enable.get() == "Enabled":
            if_RF_A = True
        else:
            if_RF_A = False

        if out_freq > 6800000000:
            channel = 1
        else:
            channel = 0

        
        d = adf5356(spi, mute_till_lock_detect_en=True, ref_doubler_en=False,
                    clkin=fref, outb_en = True, outa_en = if_RF_A, mux_out_sel = 6,
                    adc_clock=ADC_CLK, VCO_power=vco_pwr, cp_bleed=bleed_cp,
                    ADC_cov=ADC_CONV, ADC_enable=ADC_En, Counter_Reset=CT_RE,
                    cp3_state=CP_STATE, pwr_down=PWR_DOWN, pd_polarity=PD_polarity,
                    mux_level=Mux_level, autocal=Autocal, ref_mode=REF_MODE, IF_auto=if_auto)
            
        d.set_freq(out_freq, channel)
    frame_btn = tk.Frame(mainControl_window, height=100, relief=RAISED,
                        colormap="new", bd=1, padx=3, pady=3)
    frame_btn.pack(fill=X, padx=2, pady=2)
    frame_btn.place(x=220,y=240)          
    btn = Button(mainControl_window,text="start",command=clicked)
    btn.pack(fill=X, padx=2, pady=2)
    btn.place(x=400,y=240)

    radio_value_Automatic = IntVar()
    radio_value_Automatic.set(0)
    def auto():
        PowerChosen.current(3)
        RFA_Enable.current(1)
        RFB_Enable.current(0)
        radio_value_ADC_cov.set(1)
        Radio_ADC_cov_on.select()
        Radio_ADC_cov_off.deselect()
        radio_value_ADC_En.set(1)
        Radio_ADC_En_on.select()
        Radio_ADC_En_off.deselect()
        radio_value_CT_RE.set(0)
        Radio_CT_Re_on.deselect()
        Radio_CT_Re_off.select()
        radio_value_CP.set(0)
        Radio_CP_on.deselect()
        Radio_CP_off.select()
        radio_value_PWR_down.set(0)
        Radio_PWR_down_on.deselect()
        Radio_PWR_down_off.select()
        radio_value_PD.set(0)
        Radio_PD_positive.deselect()
        Radio_PD_negative.select()
        radio_value_Mux_level.set(1)
        Radio_Mux_level_3v3.select()
        Radio_Mux_level_1v8.deselect()
        radio_value_RF_mod.set(1)
        Radio_RF_mod_diff.select()
        Radio_RF_mod_single.deselect()
        radio_value_Autocal.set(1)
        Radio_Autocal_en.select()
        Radio_Autocal_dis.deselect()
        input_bleed.delete(0,END)
        ref_div_factor = 0
        f_pfd = float(input_ref.get()) * 1000000
        while True:
            ref_div_factor += 1
            f_pfd = f_pfd / ref_div_factor
            if f_pfd <= 75000000:
                break
        ref_div_factor = 0
        temp = int(float(f_pfd/61440000)*24)
        input_bleed.insert(END, temp)

        adc_clock_temp = floor((float(f_pfd/100000)-2) / 4)
        if  (float(f_pfd / 100000) - 2 ) % 4 != 0 :
            adc_clock_temp = adc_clock_temp + 1
        
        input_ADC_clk.delete(0,END)
        input_ADC_clk.insert(END, int(adc_clock_temp))
        radio_value_Automatic.set(1)
        
    def sel_Auto_dis():
        radio_value_Automatic.set(0)
    
    Radio_Auto_en = tk.Radiobutton(frame_btn, text="Automatic",
                                         variable = radio_value_Automatic,
                                         value=1, command=auto)
    Radio_Auto_en.grid(column=1, row=0)
    Radio_Auto_en.deselect()
    Radio_Auto_dis = tk.Radiobutton(frame_btn, text="Manual",
                                         variable = radio_value_Automatic,
                                         value=0, command=sel_Auto_dis)
    Radio_Auto_dis.grid(column=2, row=0)
    Radio_Auto_dis.select()
    
    
    mainControl_window.geometry('500x270')
    mainControl_window.mainloop()

def click_MainControls_55():

    mainControl_window = tk.Tk()
    mainControl_window.title("ADF5355 Main Controls")
    #reg 6
    frame_reg6 = tk.Frame(mainControl_window, height=100, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg6.pack(fill=X, padx=2, pady=2)
    frame_reg6.place(x=0,y=75)

    lbl_REG6 = tk.Label(frame_reg6,text="Register 6")
    lbl_REG6.grid(column=0, row=0)
    lbl_RFA_POWER = tk.Label(frame_reg6,text="RFoutA Power:")
    lbl_RFA_POWER.grid(column=0, row=1)
    lbl_RFA_Enable = tk.Label(frame_reg6,text="RFoutA Enable: ")
    lbl_RFA_Enable.grid(column=0, row=2)
    lbl_RFB_Enable = tk.Label(frame_reg6,text="RFoutB Enable:")
    lbl_RFB_Enable.grid(column=0, row=3)
    lbl_BLEED = tk.Label(frame_reg6,text="Bleed Current:")
    lbl_BLEED.grid(column=0, row=4)
    lbl_BLEED_1 = tk.Label(frame_reg6,text=" x 3.75uA")
    lbl_BLEED_1.grid(column=1, row=5)

    power = tk.StringVar()
    PowerChosen = ttk.Combobox(frame_reg6, width = 10, textvariable = power)
    PowerChosen['values'] = ("-4dBm", "-1dBm", "+2dBm", "+5dBm")
    PowerChosen.grid(column=1, row=1)
    PowerChosen.current(3)
    If_Enable_A = tk.StringVar()
    RFA_Enable = ttk.Combobox(frame_reg6, width = 10,
                              textvariable = If_Enable_A)
    RFA_Enable['values'] = ("Disabled", "Enabled")
    RFA_Enable.grid(column=1, row=2)
    RFA_Enable.current(1)
    If_Enable_B = tk.StringVar()
    RFB_Enable = ttk.Combobox(frame_reg6, width = 10,
                              textvariable = If_Enable_B)
    RFB_Enable['values'] = ("Enabled", "Disabled")
    RFB_Enable.grid(column=1, row=3)
    RFB_Enable.current(0)

    input_bleed = tk.Entry(frame_reg6,width=10)
    input_bleed.insert(END, '24')
    input_bleed.grid(column=0, row=5)

    
    #reg 10
    frame_reg10 = tk.Frame(mainControl_window, relief=RAISED, colormap="new",
                           bd=1, padx=3, pady=3)
    frame_reg10.pack(fill=X, padx=2, pady=2)
    frame_reg10.place(x=210,y=152)
    
    lbl_REG10 = tk.Label(frame_reg10,text="Register 10")
    lbl_REG10.grid(column=0, row=0)
    lbl_adc_clk = tk.Label(frame_reg10,text="ADC Clock:")
    lbl_adc_clk.grid(column=0, row=1)
    lbl_adc_cov = tk.Label(frame_reg10,text="ADC Conversion:")
    lbl_adc_cov.grid(column=0, row=2)
    lbl_adc_En = tk.Label(frame_reg10,text="ADC Enable:")
    lbl_adc_En.grid(column=0, row=3)
    
    input_ADC_clk = tk.Entry(frame_reg10,width=10)
    input_ADC_clk.insert(END, '154')
    input_ADC_clk.grid(column=1, row=1)

    
    radio_value_ADC_cov = IntVar()
    radio_value_ADC_cov.set(1)
    def sel_adc_cov_on():
        radio_value_ADC_cov.set(1)
        
    def sel_adc_cov_off():
        radio_value_ADC_cov.set(0)
    
    Radio_ADC_cov_on = tk.Radiobutton(frame_reg10, text="Enabled",
                                      variable = radio_value_ADC_cov, value=1,
                                      command=sel_adc_cov_on)
    Radio_ADC_cov_on.grid(column=1, row=2, sticky="w")
    Radio_ADC_cov_on.select()
    Radio_ADC_cov_off = tk.Radiobutton(frame_reg10, text="Disabled",
                                       variable = radio_value_ADC_cov, value=0,
                                       command=sel_adc_cov_off)
    Radio_ADC_cov_off.grid(column=2, row=2, sticky="w")
    Radio_ADC_cov_off.deselect()
    
    radio_value_ADC_En = IntVar()
    radio_value_ADC_En.set(1)
    def sel_adc_en_on():
        radio_value_ADC_En.set(1)
        
    def sel_adc_en_off():
        radio_value_ADC_En.set(0)
        
    Radio_ADC_En_on = tk.Radiobutton(frame_reg10, text="Enabled",
                                     variable = radio_value_ADC_En, value=1,
                                     command=sel_adc_en_on)
    Radio_ADC_En_on.grid(column=1, row=3, sticky="w")
    Radio_ADC_En_on.select()
    Radio_ADC_En_off = tk.Radiobutton(frame_reg10, text="Disabled",
                                      variable = radio_value_ADC_En, value=0,
                                      command=sel_adc_en_off)
    Radio_ADC_En_off.grid(column=2, row=3, sticky="w")
    Radio_ADC_En_off.deselect()
    
    #reg4

    frame_reg4 = tk.Frame(mainControl_window, height=100, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg4.pack(fill=X, padx=2, pady=2)
    frame_reg4.place(x=210,y=0)

    lbl_REG4 = tk.Label(frame_reg4,text="Register 4")
    lbl_REG4.grid(column=0, row=0)
    lbl_Counter_re = tk.Label(frame_reg4,text="Counter reset:")
    lbl_Counter_re.grid(column=0, row=1)
    lbl_CP3_state = tk.Label(frame_reg4,text="CP3-state:")
    lbl_CP3_state.grid(column=0, row=2)
    lbl_Powerdown = tk.Label(frame_reg4,text="Powerdown:")
    lbl_Powerdown.grid(column=0, row=3)
    lbl_PD = tk.Label(frame_reg4,text="PD Polarity:")
    lbl_PD.grid(column=0, row=4)
    lbl_Mux_level = tk.Label(frame_reg4,text="Mux level:")
    lbl_Mux_level.grid(column=0, row=5)
    lbl_RF_mod = tk.Label(frame_reg4,text="REFin Mode:")
    lbl_RF_mod.grid(column=0, row=6)

    radio_value_CT_RE = IntVar()
    radio_value_CT_RE.set(0)
    def sel_ct_re_on():
        radio_value_CT_RE.set(1)
        
    def sel_ct_re_off():
        radio_value_CT_RE.set(0)
        
    Radio_CT_Re_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                    variable = radio_value_CT_RE, value=1,
                                    command=sel_ct_re_on)
    Radio_CT_Re_on.grid(column=1, row=1, sticky="w")
    Radio_CT_Re_on.deselect()
    Radio_CT_Re_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                     variable = radio_value_CT_RE, value=0,
                                     command=sel_ct_re_off)
    Radio_CT_Re_off.grid(column=2, row=1, sticky="w")
    Radio_CT_Re_off.select()
    
    radio_value_CP = IntVar()
    radio_value_CP.set(0)
    def sel_cp_on():
        radio_value_CP.set(1)
        
    def sel_cp_off():
        radio_value_CP.set(0)
    
    Radio_CP_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                 variable = radio_value_CP, value=1,
                                 command=sel_cp_on)
    Radio_CP_on.grid(column=1, row=2, sticky="w")
    Radio_CP_on.deselect()
    Radio_CP_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                  variable = radio_value_CP, value=0,
                                  command=sel_cp_off)
    Radio_CP_off.grid(column=2, row=2, sticky="w")
    Radio_CP_off.select()
    
    radio_value_PWR_down = IntVar()
    radio_value_PWR_down.set(0)
    def sel_PWR_down_on():
        radio_value_PWR_down.set(1)
        
    def sel_PWR_down_off():
        radio_value_PWR_down.set(0)
    
    Radio_PWR_down_on = tk.Radiobutton(frame_reg4, text="Enabled",
                                       variable = radio_value_PWR_down,
                                       value=1, command=sel_PWR_down_on)
    Radio_PWR_down_on.grid(column=1, row=3, sticky="w")
    Radio_PWR_down_on.deselect()
    Radio_PWR_down_off = tk.Radiobutton(frame_reg4, text="Disabled",
                                        variable = radio_value_PWR_down,
                                        value=0, command=sel_PWR_down_off)
    Radio_PWR_down_off.grid(column=2, row=3, sticky="w")
    Radio_PWR_down_off.select()

    radio_value_PD = IntVar()
    radio_value_PD.set(0)
    def sel_PD_positive():
        radio_value_PD.set(1)
        
    def sel_PD_negative():
        radio_value_PD.set(0)
    
    Radio_PD_positive = tk.Radiobutton(frame_reg4, text="Positive",
                                       variable = radio_value_PD, value=1,
                                       command=sel_PD_positive)
    Radio_PD_positive.grid(column=1, row=4, sticky="w")
    Radio_PD_positive.deselect()
    Radio_PD_negative = tk.Radiobutton(frame_reg4, text="Negative",
                                       variable = radio_value_PD, value=0,
                                       command=sel_PD_negative)
    Radio_PD_negative.grid(column=2, row=4, sticky="w")
    Radio_PD_negative.select()

    radio_value_Mux_level = IntVar()
    radio_value_Mux_level.set(1)
    def sel_Mux_level_3v3():
        radio_value_Mux_level.set(1)
        
    def sel_Mux_level_1v8():
        radio_value_Mux_level.set(0)
    
    Radio_Mux_level_3v3 = tk.Radiobutton(frame_reg4, text="3.3V",
                                         variable = radio_value_Mux_level,
                                         value=1, command=sel_Mux_level_3v3)
    Radio_Mux_level_3v3.grid(column=1, row=5, sticky="w")
    Radio_Mux_level_3v3.select()
    Radio_Mux_level_1v8 = tk.Radiobutton(frame_reg4, text="1.8V",
                                         variable = radio_value_Mux_level,
                                         value=0, command=sel_Mux_level_1v8)
    Radio_Mux_level_1v8.grid(column=2, row=5, sticky="w")
    Radio_Mux_level_1v8.deselect()

    radio_value_RF_mod = IntVar()
    radio_value_RF_mod.set(1)
    def sel_RF_mod_diff():
        radio_value_RF_mod.set(1)
        
    def sel_RF_mod_single():
        radio_value_RF_mod.set(0)
    
    Radio_RF_mod_diff = tk.Radiobutton(frame_reg4, text="Differential",
                                         variable = radio_value_RF_mod,
                                         value=1, command=sel_RF_mod_diff)
    Radio_RF_mod_diff.grid(column=1, row=6, sticky="w")
    Radio_RF_mod_diff.select()
    Radio_RF_mod_single = tk.Radiobutton(frame_reg4, text="Single",
                                         variable = radio_value_RF_mod,
                                         value=0, command=sel_RF_mod_single)
    Radio_RF_mod_single.grid(column=2, row=6, sticky="w")
    Radio_RF_mod_single.deselect()

    
    #reg0
    frame_reg0 = tk.Frame(mainControl_window, width=200, relief=RAISED,
                          colormap="new", bd=1, padx=3, pady=3)
    frame_reg0.pack(fill=X, padx=2, pady=2)
    frame_reg0.place(x=0,y=200)
    lbl_REG0 = tk.Label(frame_reg0,text="Register 0")
    lbl_REG0.grid(column=0, row=0)
    lbl_Autocal = tk.Label(frame_reg0,text="      Autocal:          ")
    lbl_Autocal.grid(column=0, row=1)
    
    radio_value_Autocal = IntVar()
    radio_value_Autocal.set(1)
    def sel_Autocal_en():
        radio_value_Autocal.set(1)
        
    def sel_Autocal_dis():
        radio_value_Autocal.set(0)
    
    Radio_Autocal_en = tk.Radiobutton(frame_reg0, text="Enabled",
                                         variable = radio_value_Autocal,
                                         value=1, command=sel_Autocal_en)
    Radio_Autocal_en.grid(column=1, row=1, sticky="w")
    Radio_Autocal_en.select()
    Radio_Autocal_dis = tk.Radiobutton(frame_reg0, text="Disabled",
                                         variable = radio_value_Autocal,
                                         value=0, command=sel_Autocal_dis)
    Radio_Autocal_dis.grid(column=1, row=2, sticky="w")
    Radio_Autocal_dis.deselect()

    
    
    
    #rf settings
    frame_rf = tk.Frame(mainControl_window, height=100, relief=RAISED,
                        colormap="new", bd=1, padx=3, pady=3)
    frame_rf.pack(fill=X, padx=2, pady=2)
    frame_rf.place(x=0,y=0)
    lbl_RF_setting = tk.Label(frame_rf,text="RF Settings")
    lbl_RF_setting.grid(column=0, row=0)
    lbl_reference_freq = tk.Label(frame_rf,text="Reference freq:")
    lbl_reference_freq.grid(column=0, row=1)
    input_ref = tk.Entry(frame_rf,width=8)
    input_ref.insert(END, '122.88')
    input_ref.grid(column=1, row=1)
    lbl_MHz = tk.Label(frame_rf,text="MHz")
    lbl_MHz.grid(column=2, row=1)
    lbl_VCO_out = tk.Label(frame_rf,text="VCO out:")
    lbl_VCO_out.grid(column=0, row=2)
    input_vco = tk.Entry(frame_rf,width=8)
    input_vco.grid(column=1, row=2)
    lbl_MHz_1 = tk.Label(frame_rf,text="MHz")
    lbl_MHz_1.grid(column=2, row=2)

    counter = 0
        
    def clicked():
        if_auto = radio_value_Automatic.get()
            
        out_freq = int(float(input_vco.get()) * 1000000)
        ADC_CLK = int(input_ADC_clk.get())
        bleed_cp = int(input_bleed.get())
        ADC_CONV = radio_value_ADC_cov.get()
        ADC_En = radio_value_ADC_En.get()
        fref = int(float(input_ref.get()) * 1000000)
        CT_RE = radio_value_CT_RE.get()
        CP_STATE = radio_value_CP.get()
        PWR_DOWN = radio_value_PWR_down.get()
        PD_polarity = radio_value_PD.get()
        Mux_level = radio_value_Mux_level.get()
        Autocal = radio_value_Autocal.get()
        REF_MODE = radio_value_RF_mod.get()
        
        if RFA_Enable.get() == "Enabled":
            if_RF_A = True
        else:
            if_RF_A = False

        if PowerChosen.get() == "-4dBm":
            vco_pwr = 0
        elif PowerChosen.get() == "-1dBm":
            vco_pwr = 1
        elif PowerChosen.get() == "+2dBm":
            vco_pwr = 2
        elif PowerChosen.get() == "+5dBm":
            vco_pwr = 3
        
        if RFA_Enable.get() == "Enabled":
            if_RF_A = True
        else:
            if_RF_A = False

        if out_freq > 6800000000:
            channel = 1
        else:
            channel = 0

        
        d = adf5355(spi, mute_till_lock_detect_en=True, ref_doubler_en=False,
                    clkin=fref, outb_en = True, outa_en = if_RF_A, mux_out_sel = 6,
                    adc_clock=ADC_CLK, VCO_power=vco_pwr, cp_bleed=bleed_cp,
                    ADC_cov=ADC_CONV, ADC_enable=ADC_En, Counter_Reset=CT_RE,
                    cp3_state=CP_STATE, pwr_down=PWR_DOWN, pd_polarity=PD_polarity,
                    mux_level=Mux_level, autocal=Autocal, ref_mode=REF_MODE, IF_auto=if_auto)
            
        d.set_freq(out_freq, channel)
    frame_btn = tk.Frame(mainControl_window, height=100, relief=RAISED,
                        colormap="new", bd=1, padx=3, pady=3)
    frame_btn.pack(fill=X, padx=2, pady=2)
    frame_btn.place(x=220,y=240)          
    btn = Button(mainControl_window,text="start",command=clicked)
    btn.pack(fill=X, padx=2, pady=2)
    btn.place(x=400,y=240)

    radio_value_Automatic = IntVar()
    radio_value_Automatic.set(0)
    def auto():
        PowerChosen.current(3)
        RFA_Enable.current(1)
        RFB_Enable.current(0)
        radio_value_ADC_cov.set(1)
        Radio_ADC_cov_on.select()
        Radio_ADC_cov_off.deselect()
        radio_value_ADC_En.set(1)
        Radio_ADC_En_on.select()
        Radio_ADC_En_off.deselect()
        radio_value_CT_RE.set(0)
        Radio_CT_Re_on.deselect()
        Radio_CT_Re_off.select()
        radio_value_CP.set(0)
        Radio_CP_on.deselect()
        Radio_CP_off.select()
        radio_value_PWR_down.set(0)
        Radio_PWR_down_on.deselect()
        Radio_PWR_down_off.select()
        radio_value_PD.set(0)
        Radio_PD_positive.deselect()
        Radio_PD_negative.select()
        radio_value_Mux_level.set(1)
        Radio_Mux_level_3v3.select()
        Radio_Mux_level_1v8.deselect()
        radio_value_RF_mod.set(1)
        Radio_RF_mod_diff.select()
        Radio_RF_mod_single.deselect()
        radio_value_Autocal.set(1)
        Radio_Autocal_en.select()
        Radio_Autocal_dis.deselect()
        input_bleed.delete(0,END)
        ref_div_factor = 0
        f_pfd = float(input_ref.get()) * 1000000
        while True:
            ref_div_factor += 1
            f_pfd = f_pfd / ref_div_factor
            if f_pfd <= 75000000:
                break
        ref_div_factor = 0
        temp = int(float(f_pfd/61440000)*24)
        input_bleed.insert(END, temp)

        adc_clock_temp = floor((float(f_pfd/100000)-2) / 4)
        if  (float(f_pfd / 100000) - 2 ) % 4 != 0 :
            adc_clock_temp = adc_clock_temp + 1
        
        input_ADC_clk.delete(0,END)
        input_ADC_clk.insert(END, int(adc_clock_temp))
        radio_value_Automatic.set(1)
        
    def sel_Auto_dis():
        radio_value_Automatic.set(0)
    
    Radio_Auto_en = tk.Radiobutton(frame_btn, text="Automatic",
                                         variable = radio_value_Automatic,
                                         value=1, command=auto)
    Radio_Auto_en.grid(column=1, row=0)
    Radio_Auto_en.deselect()
    Radio_Auto_dis = tk.Radiobutton(frame_btn, text="Manual",
                                         variable = radio_value_Automatic,
                                         value=0, command=sel_Auto_dis)
    Radio_Auto_dis.grid(column=2, row=0)
    Radio_Auto_dis.select()
    
    
    mainControl_window.geometry('500x270')
    mainControl_window.mainloop()

def file():
    text_window = tk.Tk()
    read_me = Text(text_window)
    read_me.pack()
    fl = open("README.md", "r")
    read_me.insert(END, fl.readlines())
    fl.close()
    
    
main_window = Tk()
main_window.title("ADF5356 Controller")
quickstart_btn = Button(main_window, text="QuickStart", command = click_QuickStart)
#quickstart_btn.grid(column=1, row=1)
quickstart_btn.pack(fill=X, padx=50, pady=15)
mainControls_btn_5356 = Button(main_window, text="ADF5356 MainControls", command = click_MainControls)
mainControls_btn_5356.pack(fill=X, padx=50, pady=15)
mainControls_btn_5355 = Button(main_window, text="ADF5355 MainControls", command = click_MainControls_55)
mainControls_btn_5355.pack(fill=X, padx=50, pady=15)
readMe_btn = Button(main_window, text="Read Me", command = file)
readMe_btn.pack(fill=X, padx=50, pady=15)
main_window.geometry('300x300')
main_window.mainloop()

